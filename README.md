# mlops_study_task_3

## Описание проекта

Цель проекта — подготовить Docker образ, который можно использовать как рабочее место для экспериментов, а также в целях CI-CD и поставки зависимостей. В проекте используется комбинация Conda и PDM для управления зависимостями, что позволяет гибко настраивать рабочее окружение.

## Структура проекта

- `environment.yml`: Список зависимостей для Conda.
- `pyproject.toml`: Конфигурация для PDM, включая зависимости для разработки и продакшн.
- `Dockerfile`: Инструкции для создания Docker образа, включая настройку окружений и установку зависимостей.

## Технологии

- **Conda**: Управление зависимостями Python и настройка изолированных окружений.
- **PDM**: Современный пакетный менеджер для Python, который использует формат `pyproject.toml`.
- **Docker**: Создание и управление контейнеризированными приложениями.

## Предварительные требования

Для работы с проектом у вас должен быть установлен Docker. Все дальнейшие шаги будут выполняться в контейнере Docker.


## Сборка Docker образа

Следующая команда позволит собрать Docker образ на основе `Dockerfile` из корня проекта:

```bash
docker build -t mlops_study_task_3 .
```

## Запуск Docker образа

Для запуска собранного Docker образа используйте следующую команду:

```bash
docker run mlops_study_task_3
```