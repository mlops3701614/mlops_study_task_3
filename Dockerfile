FROM mambaorg/micromamba

COPY environment.yml /app/environment.yml
RUN micromamba env create -f /app/environment.yml

SHELL ["micromamba", "run", "-n", "prod", "/bin/bash", "-c"]

COPY . /app
WORKDIR /app

USER root
ENV PDM_IGNORE_SAVED_PYTHON=1
RUN micromamba run -n prod pdm use /opt/conda/envs/prod
RUN micromamba run -n prod pdm lock
RUN micromamba run -n prod pdm sync
RUN micromamba run -n prod pdm install

CMD ["micromamba", "run", "-n", "prod", "python", "test.py"]